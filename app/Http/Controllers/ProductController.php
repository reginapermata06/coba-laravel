<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    function list(Request $request){
        $ordering = $request->input("ordering");
        $urutan = $request->input("urutan");
        $search = $request->input("search");
        $filter = $request->input("filter");

        if($filter =="1" && $search==""){
            $data_product = Product::where("nama_product","like","%".$search."%")
                                    ->orderBy($ordering , $urutan)->paginate(5);

        }elseif($filter =="1" && $urutan=="" && $ordering==""){
            $data_product = Product::where("nama_product","like","%".$search."%")
                                    ->orderBy("nama_product","asc")->paginate(5);
                                    
        }elseif($filter =="1" && $search !=""){
            $data_product = Product::where("nama_product","like","%".$search."%")
                                    ->orderBy($ordering , $urutan)->paginate(5);
        }else{
            $data_product = Product::paginate(5);
        }

                                    // ->paginate(5);
        // }
        // if($ordering=="nama_product" && $urutan=="asc"){
        //     $data_product = Product::orderBy("nama_product","asc")
        //                             ->paginate(5);
        // }
        // if($ordering=="nama_product" && $urutan=="desc"){
        //     $data_product = Product::where("nama_product","desc")
        //                             ->paginate(5);
        // }
        // if($ordering=="urutan" && $urutan=="asc"){
        //     $data_product = Product::orderBy("urutan","asc")
        //                             ->paginate(5);
        // }
        // if($ordering=="urutan" && $urutan=="desc"){
        //     $data_product = Product::orderBy("urutan","desc")
        //                             ->paginate(5);
        // }
        
        //     $data_product = Product::orderBy("urutan","asc");
        return view("product-list")
        ->with("data_product", $data_product);

        // $data_product = Product::query();
        // if($ordering !="" && $urutan !=""){
        //     $data_product = $data_product->orderBy($ordering , $urutan);
        // }

        // if($ordering !="" && $urutan ==""){
        //     $data_product = $data_product->orderBy($ordering , "asc");
        // }
        // if($ordering =="" && $urutan !=""){
        //     $data_product = $data_product->orderBy("id" , $urutan);
        // }
        // if($ordering !=""){
        //     $data_product = $data_product->where("nama_product","like","%".$search."%");
        // }
        // $data_product = $data_product->paginate(5);
    }
    function create(){
        return view("product-create");
    }
    function save(Request $request){
        $data_product=Product::create([
            "nama_product" =>$request-> input("nama_product"),
            "slug" =>$request->input ("slug"),
            "urutan" =>$request->input ("urutan"),
            "status" =>$request->input ("status")
        ]);
        if($data_product){
            return redirect(url("product"))
            ->with("status","berhasil");
        }else{
            return redirect(url("product"))
            ->with("status","gagal");
        }
    }

    function edit($id){
        $data_product = Product::find($id);
        return view ("product-edit")
        ->with("data_product", $data_product);
    }

    function update($id, Request $request){
        $data_product = Product::find($id);
        $data_product->nama_product = $request->input("nama_product");
        $data_product->slug = $request->input("slug");
        $data_product->urutan = $request->input("urutan");
        $data_product->status = $request->input("status");
    
        $data_product->save();
        return redirect(url("product"));
    }
    
    function delete($id){
        $data_product = Product::find($id);
        $data_product-> delete();
        return redirect(url("product"));
    }
    
}
