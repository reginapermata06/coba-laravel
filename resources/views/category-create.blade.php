<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Category Create</title>
    <style>
    body{
        background-color:#3BA3BC; 
    }
    </style>
</head>
<body>
<h1 align="center"> Category Create </h1>
<form method="POST" action="{{url('category/save')}}">
@csrf
        <table width="30%" border="0" align="center">
            <tr> 
                <td>Nama Kategori</td>
                <td><input type="text" name="nama_category" placeholder="masukkan nama kategori"></td>
            </tr>
            <tr> 
                <td>Slug</td>
                <td><input type="text" name="slug"></td>
            </tr>
            <tr> 
                <td>Urutan</td>
                <td><input type="text" name="urutan"></td>
            </tr>
            <tr> 
                <td>Status</td>
                <td><label><input type="radio" name="status" value="online">Online</label><br>
                <label><input type="radio" name="status" value="offline">Offline</label><br></td>
            </tr>
            <tr> 
                <td></td>
                <td><input type="submit" value="Save"></td>
            </tr>
        </table>
    </form> 
</body>
</html>