<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            .icon {
                font-size: 150px;
                text-align: center;
            }
            html, body {
                background-color: #36A5B2;
                color: black;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: white;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
                background: #005B66;
                padding: 15px 20px;
                list-style: none;
                display: inline-table;
                : right;
            }

            .m-b-md {
                margin-bottom: 10px;
            }
            .fas{
                margin-top: 50px;
            }
            .footer{
                background: #005B66;
                text-align: center;
                height: 48px;
                width: 1335px;
                padding: 15px;
                color: white;
                font-family: 'Nunito', sans-serif;
                margin-top: 117px;
            }
        </style>
    </head>
    <body>
        <center>
            <nav class="navbar-nav mr-auto">
                <div class="links">
                        <a href="http://127.0.0.1:8000/product">Product List</a>
                        <a href="http://127.0.0.1:8000/category">Category List</a>
                        <a href="/product/create">Create Product</a>  
                        <a href="/category/create">Create Category</a>  
                        <a href="/about/list">About</a> 
                        <a href="https://github.com/Regina-Permatasari">GitHub</a>
                    </div>
            </nav>
        <center>
        <div class="icon">
        <i class="fas fa-book-open"></i>
        </div>
            <div class="content">
                <div class="title m-b-md">
                    Book Store
                </div>
                <h1> Welcome to Regina Permata Book Store </h1>
            </div>
        </div>
    </body>
<footer align="center">
    <div class="footer">
    <h4><p>Copy Right ReginaPermata06@2020</p></h4>
    </div>
</footer>
</html>
