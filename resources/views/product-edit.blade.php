<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product Edit</title>
    <style>
    body{
        background-color:#3BA3BC; 
    }
    </style>
</head>
<body>
<h1 align="center"> Product Edit </h1>
<form method="POST" action="{{url('')}}/product/update/{{$data_product->id}}">
@csrf
        <table width="30%" border="0" align="center">
            <tr> 
                <td>Nama Produk</td>
                <td><input type="text" name="nama_product" placeholder="masukkan nama produk" value="{{$data_product->nama_product}}"></td>
            </tr>
            <tr> 
                <td>Slug</td>
                <td><input type="text" name="slug" value="{{$data_product->slug}}"></td>
            </tr>
            <tr> 
                <td>Urutan</td>
                <td><input type="text" name="urutan" value="{{$data_product->urutan}}"></td>
            </tr>
            <tr> 
                <td>Status</td>
                <td><label><input type="radio" name="status" value="online">Online</label><br>
                <label><input type="radio" name="status" value="offline">Offline</label><br></td>
            </tr>
            <tr> 
                <td></td>
                <td><input type="submit" value="Update"></td>
            </tr>
        </table>
    </form> 
</body>
</html>