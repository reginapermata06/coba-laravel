<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

    <title>ProductList</title>
    <style>
    .table{
        border: 1px solid #3BA3BC;
        width: 50%;
    }
    body{
        background-color:#3BA3BC;
        font-family: sans-serif;
    }
    tr:nth-clild(even){
        background-color:grey;
    }
    </style>
</head>
<body align="center">
</form>
    <table class="table table-success table-striped" align="center">
    <h1>Daftar Produk</h1>
    <br>

    <form class="d-flex" action="{{url('product')}}">
    <input type="search" placeholder="Search...." aria-label="Search" name="search">

    <select class="form-select-sm" aria-label=".form-select-sm example" name="ordering">
    <option value="">Urutan Berdasarkan</option>
    <option value="nama_product" name="nama_product">Nama</option>
    <option value="slug" name="slug">Slug</option>
    <option value="urutan" name="urutan">Urutan</option>
    <option value="status" name="status">Status</option>
    </select>

    <select class="form-select-sm" aria-label=".form-select-sm example" name="urutan"> 
    <option value="">Urutan</option>
    <option value="asc" name="asc">Ascending</option>
    <option value="desc" name="desc">Descending</option>
    </select>
    <button class="btn btn-outline-success" type="submit" name="filter" value="1">Filter</button>
    </form>
    <br>

    <thead>
    <th>No</th>
    <th>Nama Product</th>
    <th>Slug</th>
    <th>Urutan</th>
    <th>Status</th>
    <th>Aksi</th>
    </thead>
    @foreach ($data_product as $row)
    <tr>
    <td>{{$row->id}}</td>
    <td>{{$row->nama_product}}</td>
    <td>{{$row->slug}}</td>
    <td>{{$row->urutan}}</td>
    <td>{{$row->status}}</td>
    <td>
    <div class="d-grid gap-2 d-md-flex justify-content-md-end">
    <a href="/product/edit/{{$row->id}}"><button class="btn btn-primary btn-sm">Ubah</button></a>
    <a href="/product/delete/{{$row->id}}"><button class="btn btn-secondary btn-sm">Hapus</button></a>
    </td>
    </tr>
    @endforeach   
    </table>  
    {{$data_product->appends(Request::all())->links()}}
</body>
</html>
</body>
</html>