<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

    <title>CategoryList</title>
    <style>
    .table{
        border: 1px solid #3BA3BC;
        width: 50%;
    }
    body{
        background-color:#3BA3BC;
        font-family: sans-serif;
    }
    tr:nth-clild(even){
        background-color:grey;
    }
    </style>
</head>
<body align="center">
    <table class="table table-success table-striped" align="center">
    <h1>Daftar Kategori</h1>
    <thead>
    <th>Nama Kategori</th>
    <th>Slug</th>
    <th>Urutan</th>
    <th>Status</th>
    <th>Aksi</th>
    </thead>
    @foreach ($data_category as $row)
    <tr>
    <td>{{$row->nama_category}}</td>
    <td>{{$row->slug}}</td>
    <td>{{$row->urutan}}</td>
    <td>{{$row->status}}</td>
    <td>
    <div class="d-grid gap-2 d-md-flex justify-content-md-end">
    <a href="/category/edit/{{$row->id}}"><button class="btn btn-primary btn-sm">Ubah</button></a>
    <a href="/category/delete/{{$row->id}}"><button class="btn btn-secondary btn-sm">Hapus</button></a>
    </td>
    </tr>
    @endforeach     
</body>
</html>
</body>
</html>